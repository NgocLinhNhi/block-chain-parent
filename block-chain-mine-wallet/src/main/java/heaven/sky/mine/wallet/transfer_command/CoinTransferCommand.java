package heaven.sky.mine.wallet.transfer_command;

import heaven.sky.mine.wallet.block.Block;
import heaven.sky.mine.wallet.handler.BlockChainTransferHandler;
import heaven.sky.mine.wallet.wallet.Wallet;

public class CoinTransferCommand implements ITransfer {

    private Wallet senderWallet;
    private Wallet recipientWallet;
    private Block transferBlock;

    public CoinTransferCommand(Wallet walletA, Wallet walletB, Block genesis) {
        this.senderWallet = walletA;
        this.recipientWallet = walletB;
        this.transferBlock = genesis;
    }

    @Override
    public Block transfer() {
        Block block = transferAToB(40f);
        Block block1 = transferFail(block, 1000f);
        return transferBToA(block1, 20f);
    }

    private Block transferCoin(Block previousBlock, Wallet senderWallet, Wallet recipientWallet, float value) {
        Block block = new Block(previousBlock.getHash());
        boolean result = block.addTransaction(senderWallet.sendFunds(recipientWallet.getPublicKey(), value));
        if (!result) System.out.println("Transfer coin has failed");
        BlockChainTransferHandler.addBlock(block);
        return block;
    }

    private Block transferAToB(float value) {
        System.out.println("\nWalletA's balance is: " + senderWallet.getBalance());
        System.out.println("\nWalletA is try to send funds 40 - BitCoin to WalletB...");
        Block block = transferCoin(transferBlock, senderWallet, recipientWallet, value);
        System.out.println("\nWalletA's balance is: " + senderWallet.getBalance());
        System.out.println("WalletB's balance is: " + recipientWallet.getBalance());
        return block;
    }

    private Block transferFail(Block transferBlock1, float value) {
        System.out.println("\nWalletA try to send more funds 1000 BitCoin coins to Wallet B...");
        Block block = transferCoin(transferBlock1, senderWallet, recipientWallet, value);
        System.out.println("\nWalletA's balance is: " + senderWallet.getBalance());
        System.out.println("WalletB's balance is: " + recipientWallet.getBalance());
        return block;
    }

    private Block transferBToA(Block transferBlock1, float value) {
        System.out.println("\nWalletB is try to send funds (20) to WalletA...");
        Block block = transferCoin(transferBlock1, recipientWallet, senderWallet, value);
        System.out.println("\nWalletA's balance is: " + senderWallet.getBalance());
        System.out.println("WalletB's balance is: " + recipientWallet.getBalance());
        return block;
    }

}
