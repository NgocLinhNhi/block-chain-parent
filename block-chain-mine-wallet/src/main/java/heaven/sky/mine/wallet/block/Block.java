package heaven.sky.mine.wallet.block;

import heaven.sky.block.chain.wallet.util.StringUtil;
import heaven.sky.mine.wallet.transaction.Transaction;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class Block {

    private String hash;
    private String previousHash;
    private String merkleRoot;
    private long timeStamp;
    private int nonce;

    public List<Transaction> transactions = new ArrayList<>();

    public Block(String previousHash) {
        this.previousHash = previousHash;
        this.timeStamp = new Date().getTime();
        this.hash = calculateHash();
    }

    public String calculateHash() {
        return StringUtil.applySha256(previousHash + timeStamp + nonce + merkleRoot);
    }

    //Đào coin từ các block or from wallet
    public void mineBlock(int difficulty) {
        merkleRoot = getMerkleRoot(transactions);
        String target = new String(new char[difficulty]).replace('\0', '0');
        while (!hash.substring(0, difficulty).equals(target)) {
            nonce++;
            hash = calculateHash();
        }
        System.out.println("Block Mined!!! : " + hash);
    }

    // Tacks in array of transactions and returns a merkle root.
    // Kiểm tra tính hợp lệ của block trước đó
    private String getMerkleRoot(List<Transaction> transactions) {
        int count = transactions.size();
        List<String> previousTreeLayer = new ArrayList<>();
        List<String> treeLayer = previousTreeLayer;

        for (Transaction transaction : transactions) {
            previousTreeLayer.add(transaction.transactionId);
        }

        while (count > 1) {
            treeLayer = new ArrayList<>();
            for (int i = 1; i < previousTreeLayer.size(); i++) {
                Object previousData = previousTreeLayer.get(i - 1);
                Object currentData = previousTreeLayer.get(i);
                treeLayer.add(StringUtil.applySha256(previousData.toString() + currentData));
            }
            count = treeLayer.size();
            previousTreeLayer = treeLayer;
        }
        return (treeLayer.size() == 1) ? treeLayer.get(0) : "";
    }

    // Add transactions to this block
    // Handle process transfer coin từ ví này sang ví kia
    public boolean addTransaction(Transaction transaction) {
        if (transaction == null) return false;
        if ((!previousHash.equals("0"))) {
            if ((!transaction.handleTransactionProcess())) {
                System.out.println("Transaction failed to process. Discarded.");
                return false;
            }
        }
        transactions.add(transaction);
        System.out.println("Transaction Successfully added to Block");
        return true;
    }
}