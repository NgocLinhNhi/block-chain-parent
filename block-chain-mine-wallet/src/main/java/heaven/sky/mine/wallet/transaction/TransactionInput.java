package heaven.sky.mine.wallet.transaction;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TransactionInput {
    public String transactionOutputId;
    public TransactionOutput transactionOutput;

    public TransactionInput(String transactionOutputId) {
        this.transactionOutputId = transactionOutputId;
    }

    void setTransactionOutput(TransactionOutput transactionOutput) {
        this.transactionOutput = transactionOutput;
    }
}