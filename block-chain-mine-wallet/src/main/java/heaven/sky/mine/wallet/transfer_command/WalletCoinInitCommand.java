package heaven.sky.mine.wallet.transfer_command;

import heaven.sky.mine.wallet.block.Block;
import heaven.sky.mine.wallet.handler.BlockChainTransferHandler;
import heaven.sky.mine.wallet.transaction.Transaction;
import heaven.sky.mine.wallet.transaction.TransactionOutput;
import heaven.sky.mine.wallet.wallet.Wallet;

import java.util.List;

public class WalletCoinInitCommand implements ITransfer {

    private Transaction genesisTransaction;
    private Wallet walletA;

    public WalletCoinInitCommand(Wallet walletA, Transaction genesisTransaction) {
        this.walletA = walletA;
        this.genesisTransaction = genesisTransaction;
    }

    public Transaction getGenesisTransaction() {
        return genesisTransaction;
    }

    @Override
    public Block transfer() {
        addCoinWalletA(walletA);
        return addCoinForWalletA(genesisTransaction);
    }

    private Block addCoinForWalletA(Transaction genesisTransaction) {
        System.out.println("Creating and Mining basic block... ");
        Block genesis = new Block("0");
        boolean result = genesis.addTransaction(genesisTransaction);
        if (!result) System.out.println("Add Funds for wallet A has failed");
        BlockChainTransferHandler.addBlock(genesis);
        return genesis;
    }

    private void addCoinWalletA(Wallet wallet) {
        Wallet coinBase = new Wallet();
        //create genesis transaction, which sends 100 BitCoin to walletA:
        genesisTransaction = new Transaction(
                coinBase.getPublicKey(),
                wallet.getPublicKey(),
                100f,
                null);
        genesisTransaction.generateSignature(coinBase.getPrivateKey());
        genesisTransaction.setTransactionId("0");

        List<TransactionOutput> listTranOutputs = genesisTransaction.getListTranOutputs();
        listTranOutputs.add(new TransactionOutput(
                genesisTransaction.getRecipient(),
                genesisTransaction.getValue(),
                genesisTransaction.getTransactionId())
        );

        //its important to store our first transaction in the UTXOs list.
        BlockChainTransferHandler.transactionOuts.put(
                listTranOutputs.get(0).id,
                listTranOutputs.get(0));
    }
}
