package heaven.sky.mine.wallet.transaction;

import heaven.sky.block.chain.wallet.util.StringUtil;
import lombok.Getter;
import lombok.Setter;

import java.security.PublicKey;

@Getter
@Setter
public class TransactionOutput {
    public String id;
    public PublicKey recipient;
    public float value; //the amount of coins they own
    private String parentTransactionId; //the id of the transaction this output was created in

    public TransactionOutput(PublicKey recipient, float value, String _parentTransactionId) {
        this.recipient = recipient;
        this.value = value;
        this.parentTransactionId = _parentTransactionId;
        this.id = StringUtil.applySha256(StringUtil.getStringFromKey(recipient) + value + parentTransactionId);
    }

    //Check if coin belongs to you
    public boolean isMine(PublicKey publicKey) {
        return (publicKey == recipient);
    }
}