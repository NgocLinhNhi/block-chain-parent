package heaven.sky.mine.wallet.handler;

import heaven.sky.mine.wallet.block.Block;
import heaven.sky.mine.wallet.transaction.Transaction;
import heaven.sky.mine.wallet.transaction.TransactionOutput;
import heaven.sky.mine.wallet.transfer_command.CoinTransferCommand;
import heaven.sky.mine.wallet.transfer_command.ITransfer;
import heaven.sky.mine.wallet.transfer_command.WalletCoinInitCommand;
import heaven.sky.mine.wallet.wallet.Wallet;

import java.security.Security;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BlockChainTransferHandler {

    private static final int mineDifficulty = 3;
    private Transaction genesisTransaction;

    static List<Block> blockChain;
    public static Map<String, TransactionOutput> transactionOuts;

    public BlockChainTransferHandler() {
        blockChain = new ArrayList<>();
        transactionOuts = new HashMap<>();
        initCommand();
    }

    private void initCommand() {
        createBouncy();
        Wallet walletA = new Wallet();
        Wallet walletB = new Wallet();
        Block genesis = chargeCoinWallet(walletA);
        transferCoin(walletA, walletB, genesis);
        checkValidBlockChain();
    }

    private void createBouncy() {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }


    private Block chargeCoinWallet(Wallet walletA) {
        WalletCoinInitCommand walletCoinInit = new WalletCoinInitCommand(
                walletA,
                genesisTransaction);
        Block transfer = walletCoinInit.transfer();
        genesisTransaction = walletCoinInit.getGenesisTransaction();
        return transfer;
    }

    private void transferCoin(Wallet walletA, Wallet walletB, Block blockTransfer) {
        ITransfer coinTransferCommand = new CoinTransferCommand(
                walletA,
                walletB,
                blockTransfer);
        coinTransferCommand.transfer();
    }

    public static void addBlock(Block newBlock) {
        newBlock.mineBlock(mineDifficulty);
        blockChain.add(newBlock);
    }

    private void checkValidBlockChain() {
        BlockChainValid blockChainValid = new BlockChainValid(mineDifficulty, genesisTransaction);
        System.out.println("Check BlockChain account is Valid: " + blockChainValid.isChainValid());
    }

}
