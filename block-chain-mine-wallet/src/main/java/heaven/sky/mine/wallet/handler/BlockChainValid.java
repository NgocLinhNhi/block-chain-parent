package heaven.sky.mine.wallet.handler;

import heaven.sky.mine.wallet.block.Block;
import heaven.sky.mine.wallet.transaction.Transaction;
import heaven.sky.mine.wallet.transaction.TransactionInput;
import heaven.sky.mine.wallet.transaction.TransactionOutput;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

class BlockChainValid {

    private int mineDifficulty;
    private Transaction genesisTransaction;
    private Map<String, TransactionOutput> tempTransactionOutPuts;

    BlockChainValid(int mineDifficulty, Transaction genesisTransaction) {
        this.mineDifficulty = mineDifficulty;
        this.genesisTransaction = genesisTransaction;
        tempTransactionOutPuts = new HashMap<>();
    }

    Boolean isChainValid() {
        String hashTarget = getHashTarget();
        putTemTransactionOutPut();
        return ValidateValidBlockChain(hashTarget, tempTransactionOutPuts);
    }

    private void putTemTransactionOutPut() {
        tempTransactionOutPuts.put(genesisTransaction.listTranOutputs.get(0).id, genesisTransaction.listTranOutputs.get(0));
    }

    private String getHashTarget() {
        return new String(new char[mineDifficulty]).replace('\0', '0');
    }

    private Boolean ValidateValidBlockChain(String hashTarget, Map<String, TransactionOutput> tempTransactionOutPuts) {
        Block currentBlock;
        Block previousBlock;//loop through blockChain to check hashes:

        List<Block> blockChain = BlockChainTransferHandler.blockChain;
        for (int i = 1; i < blockChain.size(); i++) {
            currentBlock = blockChain.get(i);
            previousBlock = blockChain.get(i - 1);

            if (verifyBlock(hashTarget, currentBlock, previousBlock)) return false;

            //loop  Block-Chains transactions:
            List<Transaction> transactions = currentBlock.getTransactions();
            for (int t = 0; t < transactions.size(); t++) {
                Transaction currentTransaction = transactions.get(t);

                if (verifySignatureTransaction(t, currentTransaction)) return false;
                if (validateTransactionInput(tempTransactionOutPuts, t, currentTransaction)) return false;
                if (validateTransactionOutput(tempTransactionOutPuts, t, currentTransaction)) return false;
            }
        }
        return true;
    }

    private boolean verifyBlock(String hashTarget, Block currentBlock, Block previousBlock) {
        //compare registered hash and calculated hash:
        if (!currentBlock.getHash().equals(currentBlock.calculateHash())) {
            System.out.println("#Current Hashes not equal");
            return true;
        }
        //compare previous hash and registered previous hash
        if (!previousBlock.getHash().equals(currentBlock.getPreviousHash())) {
            System.out.println("#Previous Hashes not equal");
            return true;
        }
        //check if hash is solved
        if (!currentBlock.getHash().substring(0, mineDifficulty).equals(hashTarget)) {
            System.out.println("#This block hasn't been mined");
            return true;
        }
        return false;
    }

    private boolean verifySignatureTransaction(int t, Transaction currentTransaction) {
        if (currentTransaction.verifySignature()) {
            System.out.println("#Signature on Transaction(" + t + ") is Invalid");
            return true;
        }
        if (currentTransaction.getInputsValue() != currentTransaction.getOutputsValue()) {
            System.out.println("#Inputs are note equal to listTranOutputs on Transaction(" + t + ")");
            return true;
        }
        return false;
    }

    private boolean validateTransactionOutput(Map<String, TransactionOutput> tempTransactionOutPuts,
                                              int t,
                                              Transaction currentTransaction) {
        List<TransactionOutput> listTranOutputs = currentTransaction.getListTranOutputs();
        for (TransactionOutput output : listTranOutputs) {
            tempTransactionOutPuts.put(output.getId(), output);
        }

        if (listTranOutputs.get(0).getRecipient() != currentTransaction.getRecipient()) {
            System.out.println("#Transaction(" + t + ") output recipient is not who it should be");
            return true;
        }
        if (listTranOutputs.get(1).getRecipient() != currentTransaction.getSender()) {
            System.out.println("#Transaction(" + t + ") output 'change' is not sender.");
            return true;
        }
        return false;
    }

    private boolean validateTransactionInput(Map<String, TransactionOutput> tempTransactionOutPuts,
                                             int t,
                                             Transaction currentTransaction) {
        TransactionOutput tempTransactionOutput;
        for (TransactionInput input : currentTransaction.getListTranInputs()) {
            tempTransactionOutput = tempTransactionOutPuts.get(input.getTransactionOutputId());
            if (tempTransactionOutput == null) {
                System.out.println("#Referenced input on Transaction(" + t + ") is Missing");
                return true;
            }
            if (input.getTransactionOutput().getValue() != tempTransactionOutput.getValue()) {
                System.out.println("#Referenced input Transaction(" + t + ") value is Invalid");
                return true;
            }
            tempTransactionOutPuts.remove(input.getTransactionOutputId());
        }
        return false;
    }

}
