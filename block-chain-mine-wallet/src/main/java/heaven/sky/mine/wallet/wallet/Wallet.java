package heaven.sky.mine.wallet.wallet;

import heaven.sky.mine.wallet.handler.BlockChainTransferHandler;
import heaven.sky.mine.wallet.transaction.Transaction;
import heaven.sky.mine.wallet.transaction.TransactionInput;
import heaven.sky.mine.wallet.transaction.TransactionOutput;
import lombok.Getter;
import lombok.Setter;

import java.security.*;
import java.security.spec.ECGenParameterSpec;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
public class Wallet {
    public PrivateKey privateKey;
    public PublicKey publicKey;
    //only mapTranOutputs owned by this wallet.
    private Map<String, TransactionOutput> mapTranOutputs = new HashMap<>();

    public Wallet() {
        generateKeyPair();
    }

    //Generate Private key /public key
    private void generateKeyPair() {
        try {
            KeyPair keyPair = createKeyPair();
            privateKey = keyPair.getPrivate();
            publicKey = keyPair.getPublic();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private KeyPair createKeyPair() throws NoSuchAlgorithmException, NoSuchProviderException, InvalidAlgorithmParameterException {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("ECDSA", "BC");
        SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
        ECGenParameterSpec ecSpec = new ECGenParameterSpec("prime192v1");
        keyGen.initialize(ecSpec, random);
        return keyGen.generateKeyPair();
    }

    // returns balance and stores the transactionOutput's owned by this wallet in this.transactionOutput
    public float getBalance() {
        float total = 0;
        for (Map.Entry<String, TransactionOutput> item : BlockChainTransferHandler.transactionOuts.entrySet()) {
            TransactionOutput transactionOutput = item.getValue();
            // check : if output belongs to me ( if coins belong to me )
            if (transactionOutput.isMine(publicKey)) {
                mapTranOutputs.put(transactionOutput.getId(), transactionOutput);
                total += transactionOutput.getValue();
            }
        }
        return total;
    }

    // Generates and returns a new transaction from this wallet.
    public Transaction sendFunds(PublicKey _recipient, float value) {
        if (getBalance() < value) { // gather balance and check funds.
            System.out.println("#Not Enough funds to send transaction. Transaction Discarded.");
            return null;
        }

        List<TransactionInput> inputs = new ArrayList<>();
        addTranInput(value, inputs);
        Transaction newTransaction = new Transaction(publicKey, _recipient, value, inputs);
        newTransaction.generateSignature(privateKey);
        removeTranOutPut(inputs);

        return newTransaction;
    }

    private void addTranInput(float value, List<TransactionInput> inputs) {
        float total = 0;
        for (Map.Entry<String, TransactionOutput> item : mapTranOutputs.entrySet()) {
            TransactionOutput transactionOutput = item.getValue();
            total += transactionOutput.getValue();
            inputs.add(new TransactionInput(transactionOutput.getId()));
            if (total > value) break;
        }
    }

    private void removeTranOutPut(List<TransactionInput> inputs) {
        for (TransactionInput input : inputs) {
            mapTranOutputs.remove(input.getTransactionOutputId());
        }
    }
}