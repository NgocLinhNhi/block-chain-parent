package heaven.sky.mine.wallet.transfer_command;

import heaven.sky.mine.wallet.block.Block;

public interface ITransfer {
    Block transfer();
}
