package heaven.sky.mine.wallet.transaction;

import heaven.sky.block.chain.wallet.util.StringUtil;
import heaven.sky.mine.wallet.handler.BlockChainTransferHandler;
import lombok.Getter;
import lombok.Setter;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class Transaction {

    public String transactionId; // this is also the hash of the transaction
    public PublicKey sender; // senders address/public key.
    public PublicKey recipient; // Recipients address/public key.
    public float value;
    private byte[] signature; // this is to prevent anybody else from spending

    public static float minimumTransaction = 0.1f;

    public List<TransactionInput> listTranInputs;
    public List<TransactionOutput> listTranOutputs = new ArrayList<>();

    private static int sequence = 0; // a rough count of how many transactions

    public Transaction(PublicKey from, PublicKey to, float value, List<TransactionInput> inputs) {
        this.sender = from;
        this.recipient = to;
        this.value = value;
        this.listTranInputs = inputs;
    }

    // This Calculates the heaven.sky.wallet.transaction hash (which will be used as its Id)
    private String calculateHash() {
        sequence++; // increase the sequence to avoid 2 identical transactions
        String data = StringUtil.getStringFromKey(sender) + StringUtil.getStringFromKey(recipient) + value + sequence;
        return StringUtil.applySha256(data);
    }

    // Signs all the data we dont wish to be tampered with.
    //tạo chữ ký = applyECDSASig
    public void generateSignature(PrivateKey privateKey) {
        String data = StringUtil.getStringFromKey(sender) + StringUtil.getStringFromKey(recipient) + value;
        signature = StringUtil.applyECDSASig(privateKey, data);
    }

    // Verify the data we signed has not been tampered with
    //xác thực chữ ký  = verifyECDSASig
    public boolean verifySignature() {
        String data = StringUtil.getStringFromKey(sender) + StringUtil.getStringFromKey(recipient) + value;
        return !StringUtil.verifyECDSASig(sender, data, signature);
    }

    public boolean handleTransactionProcess() {

        if (verifySignature()) {
            System.out.println("#Transaction Signature failed to verify");
            return false;
        }

        // gather transaction listTranInputs (Make sure they are unspent):
        for (TransactionInput transactionInput : listTranInputs) {
            transactionInput.setTransactionOutput(
                    BlockChainTransferHandler.transactionOuts.get(transactionInput.getTransactionOutputId())
            );
        }

        // check if transaction is valid:
        if (getInputsValue() < minimumTransaction) {
            System.out.println("#Transaction Inputs to small: " + getInputsValue());
            return false;
        }

        // generate transaction listTranOutputs:
        float leftOver = getInputsValue() - value; // get value of listTranInputs then the left over change:
        transactionId = calculateHash();
        listTranOutputs.add(new TransactionOutput(this.recipient, value, transactionId)); // send value to recipient
        listTranOutputs.add(new TransactionOutput(this.sender, leftOver, transactionId)); // send the left over 'change' back to sender

        // add listTranOutputs to Unspent list
        for (TransactionOutput transactionOutput : listTranOutputs) {
            BlockChainTransferHandler.transactionOuts.put(transactionOutput.getId(), transactionOutput);
        }

        // remove transaction listTranInputs from transactionOutput lists as spent:
        for (TransactionInput transactionInput : listTranInputs) {
            if (transactionInput.getTransactionOutput() == null) continue; // if Transaction can't be found skip it
            BlockChainTransferHandler.transactionOuts.remove(transactionInput.getTransactionOutput().getId());
        }

        return true;
    }

    // returns sum of listTranInputs(UTXOs) values
    public float getInputsValue() {
        float total = 0;
        for (TransactionInput transactionInput : listTranInputs) {
            if (transactionInput.getTransactionOutput() == null) continue; // if Transaction can't be found skip it
            total += transactionInput.getTransactionOutput().getValue();
        }
        return total;
    }

    // returns sum of listTranOutputs:
    public float getOutputsValue() {
        float total = 0;
        for (TransactionOutput transactionOutput : listTranOutputs) {
            total += transactionOutput.getValue();
        }
        return total;
    }
}
