package heaven.sky.block_chain.mine.handler;

import com.google.gson.GsonBuilder;
import heaven.sky.block_chain.mine.block.Block;

import java.util.ArrayList;

public class BlockChainMiningHandler {

    private static ArrayList<Block> blockchain = new ArrayList<>();
    // set số càng to đào càng lâu ...
    // 5 chỉ 1800ms -> 2000ms
    // 6 thì lâu vcl hơn => 22.263 ms trung bình = 22s
    // 10 chạy nguyên ngày đào được 1 block / 50 = vài tháng với laptop máy mình =))
    // => lên chỉ 1 đơn vị là đã thấy vỡ mặt rồi
    private static int difficulty = 6;

    public static void main(String[] args) {
        // add our blocks to the blockChain ArrayList:
        mineBlock("Hi im the first block", "Trying to Mine block 1...", 0);
        mineBlock("Yo im the second block", "Trying to Mine block 2... ", 1);
        mineBlock("Hey im the third block", "Trying to Mine block 3... ", 2);

        System.out.println("\nBlockchain is Valid: " + isChainValid());

        String blockChainJson = new GsonBuilder().setPrettyPrinting().create().toJson(blockchain);
        System.out.println("The block chain data:" + blockChainJson);
    }

    private static void mineBlock(String contentBlock, String blockName, int i) {
        long startTime = System.currentTimeMillis();

        if (i == 0) blockchain.add(new Block(contentBlock, "0"));
        else blockchain.add(new Block(contentBlock, blockchain.get(blockchain.size() - 1).hash));

        System.out.println(blockName);
        Block block = blockchain.get(i);
        block.mineBlock(difficulty);

        long endTime = System.currentTimeMillis();
        long timeHandle = endTime - startTime;
        System.out.println("Time for mining block " + i + " : in " + timeHandle + "(ms)");
    }

    private static Boolean isChainValid() {
        Block currentBlock;
        Block previousBlock;
        String hashTarget = new String(new char[difficulty]).replace('\0', '0');

        for (int i = 1; i < blockchain.size(); i++) {
            currentBlock = blockchain.get(i);
            previousBlock = blockchain.get(i - 1);
            // compare registered hash and calculated hash:
            if (!currentBlock.hash.equals(currentBlock.calculateHash())) {
                System.out.println("Current Hashes not equal");
                return false;
            }
            // compare previous hash and registered previous hash
            if (!previousBlock.hash.equals(currentBlock.previousHash)) {
                System.out.println("Previous Hashes not equal");
                return false;
            }
            //check if hash is solved
            if (!currentBlock.hash.substring(0, difficulty).equals(hashTarget)) {
                System.out.println("This block hasn't been mined");
                return false;
            }
        }
        return true;
    }
}