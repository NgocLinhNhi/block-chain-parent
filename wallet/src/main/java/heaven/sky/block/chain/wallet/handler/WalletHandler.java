package heaven.sky.block.chain.wallet.handler;

import heaven.sky.block.chain.wallet.transaction.Transaction;
import heaven.sky.block.chain.wallet.util.StringUtil;
import heaven.sky.block.chain.wallet.wallet.Wallet;

import java.security.Security;

public class WalletHandler {
    private static Wallet walletA;
    private static Wallet walletB;

    public static void main(String[] args) {
        // Setup Bouncy castle as a Security Provider
        createBouncyCastle();
        initWallet();
        // Test public and private keys
        System.out.println("Private key : " + StringUtil.getStringFromKey(walletA.privateKey));
        System.out.println("Public key :" + StringUtil.getStringFromKey(walletA.publicKey));
        createTransaction();
    }

    private static void createBouncyCastle(){
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }

    private static void createTransaction() {
        // Create a heaven.sky.wallet.handler heaven.sky.wallet.transaction from WalletHandler to walletB
        Transaction transaction = new Transaction(walletA.publicKey, walletB.publicKey, 5, null);
        transaction.generateSignature(walletA.privateKey);
        // Verify the signature works and verify it from the public key
        System.out.println("Is signature verified " + transaction.verifySignature());
    }

    private static void initWallet() {
        // Create the new wallets
        walletA = new Wallet();
        walletB = new Wallet();
    }
}
