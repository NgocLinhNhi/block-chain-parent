package heaven.sky.block.chain.wallet.transaction;

import heaven.sky.block.chain.wallet.util.StringUtil;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.ArrayList;

public class Transaction {

    private String transactionId; // this is also the hash of the heaven.sky.wallet.transaction.
    public PublicKey sender; // senders address/public key.
    public PublicKey recipient; // Recipients address/public key.
    public float value;
    public byte[] signature; // this is to prevent anybody else from spending
    // funds in our heaven.sky.wallet.wallet.

    public ArrayList inputs = new ArrayList();
    public ArrayList outputs = new ArrayList();

    private static int sequence = 0; // a rough count of how many transactions

    public Transaction(PublicKey from, PublicKey to, float value, ArrayList inputs) {
        this.sender = from;
        this.recipient = to;
        this.value = value;
        this.inputs = inputs;
    }

    // This Calculates the heaven.sky.wallet.transaction hash (which will be used as its Id)
    private String calculateHash() {
        sequence++; // increase the sequence to avoid 2 identical transactions
        String data = StringUtil.getStringFromKey(sender) + StringUtil.getStringFromKey(recipient) + value + sequence;
        return StringUtil.applySha256(data);
    }

    // Signs all the data we dont wish to be tampered with.
    //tạo chữ ký = applyECDSASig
    public void generateSignature(PrivateKey privateKey) {
        String data = StringUtil.getStringFromKey(sender) + StringUtil.getStringFromKey(recipient) + value;
        signature = StringUtil.applyECDSASig(privateKey, data);
    }

    // Verify the data we signed has not been tampered with
    //xác thực chữ ký  = verifyECDSASig
    public boolean verifySignature() {
        String data = StringUtil.getStringFromKey(sender) + StringUtil.getStringFromKey(recipient) + value;
        return StringUtil.verifyECDSASig(sender, data, signature);
    }
}
