package heaven.sky.block.chain.wallet.wallet;

import java.security.*;
import java.security.spec.ECGenParameterSpec;

public class Wallet {
    // Tạo public key + private key cho ví điện tử person
    // Các key được mã hóa từ thuật toán ECDSA
    public PrivateKey privateKey;
    public PublicKey publicKey;

    public Wallet() {
        generateKeyPair();
    }

    private void generateKeyPair() {
        try {
            KeyPairGenerator keyGen = KeyPairGenerator.getInstance("ECDSA", "BC");
            SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
            ECGenParameterSpec ecSpec = new ECGenParameterSpec("prime192v1");
            // Initialize the key generator and generate a KeyPair
            // 256 bytes provides an
            keyGen.initialize(ecSpec, random);
            // acceptable security level
            KeyPair keyPair = keyGen.generateKeyPair();
            // Set the public and private keys from the keyPair
            privateKey = keyPair.getPrivate();
            publicKey = keyPair.getPublic();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}